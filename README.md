# Mew Discord Bot

This bot was made to help hosting raids on discord.

## Requirements
- Python 3.7+
- pip

## Installation
`pip install -r requirements.txt`

## Running the bot
`python mew.py`

## Available commands
  - **den**: Show the shiny sprites of the pokemons in the den.
  - **fc**: Create a Friend Code Sharing embed that self-destruct.
  - **help**: Shows the bot commands description.
  - **info**: Gives information about the bot.
  - **raid**: Create a raid embed with the raid information.
  - **raid_banner**:Create a Raid embed with the defined code and banner.
  - **rraid**: Create a raid embed with the raid information using a random code.
  - **rraid_banner**: Create a Raid embed with a random code and the provided banner url.
