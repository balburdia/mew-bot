"""Mew bot for Pokemon Raid Management."""
import os
from datetime import datetime, timezone
from random import randint
from random import seed
from typing import Optional

import discord
from discord.ext import commands
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

VERSION = "0.1.1"
PREFIX = "?"
GAME_VERSION = {
    'sw': 'Sword',
    'sh': 'Shield'
}


# Define the bot commands prefix
bot = commands.Bot(command_prefix=PREFIX)

# Set the random seed.
seed(42)


def generate_random_code() -> str:
    """Generate a random code between 0000 and 9999."""
    return str(randint(0, 9999)).zfill(4)


def create_base_embed(ctx, title: str, description: str = "") -> discord.Embed:
    """Create the base embed for bot answers."""
    # Create the Embed object.
    embed = discord.Embed(
        title=title, description=description,
        color=discord.Colour.dark_magenta(),
        timestamp=datetime.now(timezone.utc))

    # Get the username.
    author = str(ctx.message.author).split('#')[0]
    # Set the command author in the embed.
    embed.set_author(name=author, icon_url=ctx.message.author.avatar_url)

    # Set the embed thumbnail
    embed.set_thumbnail(url=ctx.message.guild.icon_url)

    return embed


def create_raid_embed(ctx, *, code: str, pokemon: str = None,
                      extra: Optional[str] = None,
                      banner_url: Optional[str] = None) -> discord.Embed:
    """Create a discord embed to return to the bot."""
    extra_inline = False
    # Create the base embed
    embed = create_base_embed(ctx, title="RAID INITIATED!")

    # Add the raid code
    embed.add_field(name="Code", value=code)

    # If the banner is not set, add the additional information
    if banner_url is None:
        # Add the Pokemon information
        embed.add_field(name="Pokemon", value=pokemon)
    # If the banner is set, add the image to the embed
    else:
        extra_inline = True
        embed.set_image(url=banner_url)

    # Add the extra information if available
    if extra is not None:
        embed.add_field(name="Extra information",
                        value=extra, inline=extra_inline)

    return embed


@bot.command(name='info',
             help='Gives information about the bot.')
async def info(ctx):
    """Create a embed with the bot information."""
    # Create the base embed
    embed = create_base_embed(
        ctx, title="Mew bot",
        description="Made by a lazy host, to lazy hosts.")

    # Add the author info
    embed.add_field(name="Author", value="Balbs")

    # Add the current bot version
    embed.add_field(name="Version", value=VERSION)

    # Add the gitlab link to the bot
    embed.add_field(name="Open Source Repository",
                    value='https://gitlab.com/balburdia/mew-bot',
                    inline=False)

    # Get Mew sprite image
    file = discord.File(f"static/img/mew.png", filename='mew.png')

    # Add shiny sprites
    embed.set_image(url="attachment://mew.png")

    # Send the answer to the server
    await ctx.send(file=file, embed=embed)


@bot.command(name='raid',
             help='Create a raid embed with the raid information.')
async def raid(ctx, code: str, pokemon: str, extra: Optional[str] = None):
    """Create a raid embed with the raid information."""
    # Delete the trigger message
    await ctx.message.delete(delay=0.5)

    # Create the raid embed
    embed = create_raid_embed(ctx, code=code, pokemon=pokemon, extra=extra)

    # Send the answer to the server
    await ctx.send(embed=embed)


@bot.command(name='raid_banner',
             help='Create a Raid embed with the defined code and banner.')
async def raid_banner(ctx, code: str, banner_url: str,
                      extra: Optional[str] = None):
    """Create a raid embed with the raid information."""
    # Delete the trigger message
    await ctx.message.delete(delay=0.5)

    # Create the raid embed with a banner
    embed = create_raid_embed(
        ctx, code=code, banner_url=banner_url, extra=extra)

    # Send the answer to the server
    await ctx.send(embed=embed)


@bot.command(name='rraid',
             help='Create a raid embed with the raid '
                  'information using a random code.')
async def rraid(ctx, pokemon: str, extra: Optional[str] = None):
    """Create a raid embed with the raid information using a random code."""
    # Delete the trigger message
    await ctx.message.delete(delay=0.5)

    # Generate the random code between 0000 and 9999
    code = generate_random_code()

    # Create the raid embed with the provided information
    embed = create_raid_embed(ctx, code=code, pokemon=pokemon, extra=extra)

    # Send the answer to the server
    await ctx.send(embed=embed)


@bot.command(name='rraid_banner',
             help='Create a Raid embed with a random code'
                  'and the provided banner.')
async def rraid_banner(ctx, banner_url: str, extra: Optional[str] = None):
    """Create a Raid embed with a random code and the provided banner."""
    # Delete the trigger message
    await ctx.message.delete(delay=0.5)

    # Generate the random code between 0000 and 9999
    code = generate_random_code()

    # Create the raid embed with the provided information
    embed = create_raid_embed(
        ctx, code=code, banner_url=banner_url, extra=extra)

    # Send the answer to the server
    await ctx.send(embed=embed)


@bot.command(name='fc',
             help='Create a Friend Code Sharing embed that self-destruct.')
async def fc(ctx, friend_code: str, ttl: int = 60):
    """Create a Friend Code Sharing embed that self-destruct."""
    # Delete the trigger message
    await ctx.message.delete(delay=0.5)

    # Create the base embed
    embed = create_base_embed(ctx, title="FRIEND CODE SHARING!")

    # Add the friend code
    embed.add_field(name="Friend Code", value=friend_code, inline=False)

    # Add the warning about message deletion
    embed.add_field(name="Warning",
                    value=f"This message will be deleted in {ttl} seconds.")

    # Send the answer to the server
    message = await ctx.send(embed=embed)

    # Delete the created embed after ttl seconds.
    await message.delete(delay=ttl)


@bot.command(name='den',
             help='Show the shiny sprites of the pokemons in the den.')
async def den(ctx, number: int, version: Optional[str] = 'sw'):
    """Show the shiny sprites of the pokemons in the requested den."""
    # Delete the trigger message
    await ctx.message.delete(delay=0.5)

    # Check if the den number is valid
    if number not in range(1, 94):
        await ctx.send("Please provide a valid den number (1-93).")
        return

    # Set Sword as a fallback in case of a wrong version provided
    if version not in GAME_VERSION.keys():
        version = 'sw'

    # Create the base embed
    embed = create_base_embed(ctx, title="SHINY SPRITES!")

    # Add the den number
    embed.add_field(name="Den Number", value=number)

    # Add the game version
    embed.add_field(name="Game Version", value=GAME_VERSION.get(version))

    # Get shiny sprite image
    filename = f'{number}{version}.png'
    file = discord.File(f"static/img/{filename}", filename=filename)

    # Add shiny sprites
    embed.set_image(url=f"attachment://{filename}")

    # Send the answer to the server
    await ctx.send(file=file, embed=embed)


@bot.event
async def on_command_error(ctx, error):
    """Error handler function."""
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send("Required command argument missing. Use "
                       f"`{PREFIX}help {ctx.command.name}` to see the "
                       "description and usage for this command.")
    if isinstance(error, commands.ExpectedClosingQuoteError):
        emoji = discord.utils.get(bot.emojis, name="pausechamp") or ""
        await ctx.send(f"You forgot to close the quote! {emoji}")


@bot.event
async def on_ready():
    await bot.change_presence(
        status=discord.Status.online,
        activity=discord.Game("Hosting shiny raids!"))
    print('Bot is ready!')


bot.run(TOKEN)
